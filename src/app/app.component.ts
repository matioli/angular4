import {Component} from '@angular/core';
import {IUsuario} from '../interfaces/interface.usuario';

export class Usuario implements IUsuario {
  idUsuario: string;
  nome: string;
  email: string;
  senha: string;
  situacao: string;
  token: string;
  criado_em: string;
  facebook_id: string;
  google_id: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Sistema de Boletim';

  usuario: Usuario = {
    idUsuario: '1',
    nome: 'Matioli',
    email: 'meu@email.com',
    senha: 'minhasenha',
    situacao: '1',
    token: '"meu token, se houver',
    criado_em: '',
    facebook_id: '',
    google_id: ''
  };

}
