
export interface IUsuario {
  idUsuario: string;
  nome: string;
  email: string;
  senha: string;
  situacao: string;
  token: string;
  criado_em: string;
  facebook_id: string;
  google_id: string;
}
