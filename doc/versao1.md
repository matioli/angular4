Neste branch:
- Defini um interface para a classe Usuario, chamada IUsuario (interfaces/usuario.interface.ts)
  export interface IUsuario {
    idUsuario: string;
    nome: string;
    email: string;
    senha: string;
    situacao: string;
    token: string;
    criado_em: string;
    facebook_id: string;
    google_id: string;
  }
- Declarei um objeto do tipo Usuario e setei seus atributos
  usuario: Usuario = {
    idUsuario: '1',
    nome: 'Matioli',
    email: 'meu@email.com',
    senha: 'minhasenha',
    situacao: '1',
    token: '"meu token, se houver',
    criado_em: '',
    facebook_id: '',
    google_id: ''
  };
- Instalei bootstrap4, jquery e tether
  npm install bootstrap@v4.0.0-alpha.6 jquery tether
- configurei angular-cli.json para usar bootstrat
     "styles": [
        "styles.css",
        "../node_modules/bootstrap/dist/css/bootstrap.css"
      ],
      "scripts": [
        "../node_modules/jquery/dist/jquery.js",
        "../node_modules/tether/dist/js/tether.js",
        "../node_modules/bootstrap/dist/js/bootstrap.js"
      ],
- Alterei o templante do app.component para demonstrar duas coisas:
  1) Que o bootstrap está funcionando;
  2) Data binding, que é a ligação de dados entre o model (classe) e a view (template)
